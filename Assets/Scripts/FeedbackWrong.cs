﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FeedbackWrong : MonoBehaviour {

	private static FeedbackWrong instance;

	private static string[] content = { "Lihat bintang lain", "Bukan bintang ini", "Kurang tepat" };

	public static void DisplayFeedback() {
		instance.StartDisplayingFeedback ();
	}

	public Text textFeedback;

	private bool isDisplayed;
	private float timestampStartDisplay;

	// Use this for initialization
	void Start () {
		instance = this;
		gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (isDisplayed && Time.time - timestampStartDisplay >= 1f) {
			gameObject.SetActive (false);
			textFeedback.gameObject.SetActive (false);
			isDisplayed = false;
		}
	}

	void StartDisplayingFeedback() {
		timestampStartDisplay = Time.time;
		gameObject.SetActive (true);
		textFeedback.gameObject.SetActive (true);
		textFeedback.text = content [Random.Range (0, content.Length)];
		isDisplayed = true;
	}
}
