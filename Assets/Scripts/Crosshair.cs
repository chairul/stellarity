﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Crosshair : MonoBehaviour {

	public static Crosshair instance;

	private float rotationAngle = -1f;
	private Image imageCrosshair;
	private Transform thisTransform;

	public Transform mainCamera;
	public Image imageGazeBar;

	public Sprite spriteCrosshairWhite;
	public Sprite spriteGazeBarWhite;
	public Sprite spriteCrosshairBlack;
	public Sprite spriteGazeBarBlack;

	// Use this for initialization
	void Start () {
		instance = this;
		imageCrosshair = GetComponent<Image> ();

		thisTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
//		Vector3 newPosition = mainCamera.position;
//		newPosition.y += 1;
//		newPosition.z += 1;
//
//		transform.position = newPosition;
	}

	public void AdjustGazeFeedback(float gazeTimeComparison) {
		imageCrosshair.color = new Color (1f, 1f, 1f, 0.5f + (gazeTimeComparison / 2));
		imageGazeBar.fillAmount = gazeTimeComparison;

		Vector3 eulerAngle = thisTransform.localEulerAngles;

		if (gazeTimeComparison == 0f) {
			eulerAngle.z = 0f;
			thisTransform.localEulerAngles = eulerAngle;

			rotationAngle = -1f;
		} else {
			eulerAngle.z += rotationAngle;
			thisTransform.localEulerAngles = eulerAngle;

			rotationAngle -= 0.2f;
		}
	}

	public void SetDarkMode() {
		imageCrosshair.sprite = spriteCrosshairBlack;
		imageGazeBar.sprite = spriteGazeBarBlack;
	}

	public void SetLightMode() {
		imageCrosshair.sprite = spriteCrosshairWhite;
		imageGazeBar.sprite = spriteGazeBarWhite;
	}
}
