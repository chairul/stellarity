﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIStartButton : InteractiveObject {

	public Sprite spriteActive;
	public Sprite spriteInactive;

	void Start() {
		isInteractable = true;
		SoundManager.referenceCamera = GameObject.Find ("CardboardMain/Head/Main Camera");
	}

	protected override void ActionOnSelected ()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Tutorial");
		SoundManager.PlaySFXOneShot (SoundManager.clipSFXButton, true);
	}

	protected override void ActionOnGazeStarted ()
	{
		Crosshair.instance.SetDarkMode ();
		GetComponent<Image> ().sprite = spriteActive;
	}

	protected override void ActionOnGazeEnded ()
	{
		Crosshair.instance.SetLightMode ();
		GetComponent<Image> ().sprite = spriteInactive;
	}
}
