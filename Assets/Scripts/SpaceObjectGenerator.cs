﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SpaceObjectGenerator : MonoBehaviour {

	public GameObject prefabSpaceObject;
	public Sprite[] cloudImages;
	public Sprite[] starImages;
	public Sprite[] blackSpots;
	public GameObject[] listOfConstellation;
	public GameObject[] listOfHUD;
	public GameObject logo;
	public Text textProgress;

	private const int NUM_OF_CLOUDS = 30;
	private const int NUM_OF_STARS = 30;
	private const int NUM_OF_BLACKSPOTS = 20;

	private List<GameObject> activeConstellation;
	private List<GameObject> poolOfConstellation;
	private float timestampGameStarted;
	private bool isGameStarted;
	private int countFinishedConstellation;

	public AudioSource soundIntro;
	public AudioSource soundAmbience;
	public AudioSource soundBGM;

	private bool isAmbiencePlaying;
	private bool isBGMPlaying;
	private bool hasJustFinishedTutorial;

	public bool isTimetrialMode;
	public bool isTimetrialChallenge;
	private bool isTimetrialStarted;
	private float timestampStartTimetrial;
	private const float TIMETRIAL_DURATION = 180f;
	private const float TIMETRIAL_START_DELAY = 4f;

	public GameObject panelTime;
	public GameObject panelResult;
	public Text textHint;
	public Text textTime;
	public Text textResult;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < NUM_OF_CLOUDS; i++) {
			GameObject newCloud = (GameObject)Instantiate (prefabSpaceObject);
			newCloud.transform.SetParent (transform);

			Vector3 direction = Random.onUnitSphere;
			float distance = Random.Range (7f, 9f);

			newCloud.transform.localPosition = direction * distance;
			newCloud.transform.LookAt (transform);

			newCloud.GetComponent<SpriteRenderer> ().sprite = cloudImages [Random.Range (0, cloudImages.Length)];

			Color cloudColor = newCloud.GetComponent<SpriteRenderer> ().color;
			cloudColor.a = 0.5f;
			newCloud.GetComponent<SpriteRenderer> ().color = cloudColor;
		}

		for (int i = 0; i < NUM_OF_STARS; i++) {
			GameObject newStar = (GameObject)Instantiate (prefabSpaceObject);
			newStar.transform.SetParent (transform);

			Vector3 direction = Random.onUnitSphere;
			float distance = Random.Range (7f, 9f);

			newStar.transform.localPosition = direction * distance;
			newStar.transform.LookAt (transform);

			newStar.GetComponent<SpriteRenderer> ().sprite = starImages [Random.Range (0, starImages.Length)];
		}

		for (int i = 0; i < NUM_OF_BLACKSPOTS; i++) {
			GameObject newBlackspot = (GameObject)Instantiate (prefabSpaceObject);
			newBlackspot.transform.SetParent (transform);

			Vector3 direction = Random.onUnitSphere;

			if (i == NUM_OF_BLACKSPOTS - 1) {
				direction = new Vector3 (0f, 1f, 0f);
			} else if (i == NUM_OF_BLACKSPOTS - 2) {
				direction = new Vector3 (0f, -1f, 0f);
			}

			float distance = Random.Range (7f, 9f);

			newBlackspot.transform.localPosition = direction * distance;
			newBlackspot.transform.LookAt (transform);

			newBlackspot.GetComponent<SpriteRenderer> ().sprite = blackSpots [Random.Range (0, blackSpots.Length)];
			newBlackspot.GetComponent<SpriteRenderer> ().sortingLayerName = "Background";
		}

		poolOfConstellation = new List<GameObject> ();

		timestampGameStarted = Time.time;

		textProgress.text = countFinishedConstellation.ToString () + " / " + listOfConstellation.Length.ToString ();

		for (int i = 0; i < listOfHUD.Length; i++) {
			listOfHUD [i].SetActive (false);
		}

		soundIntro.GetComponent<SoundComponent> ().OnAudioFinished += HandleOnSoundIntroFinished;
		soundAmbience.GetComponent<SoundComponent> ().OnAudioFinished += HandleOnSoundAmbienceFinished;

		SoundManager.referenceCamera = GameObject.Find ("CardboardMain/Head/Main Camera");

		if (!isTimetrialMode) {
			TutorialManager.isTutorialActive = true;
		}


		if (isTimetrialChallenge) {
			for (int i = 0; i < transform.childCount; i++) {
				transform.GetChild (i).GetComponent<Constellation> ().OnConstellationFinished += HandleOnConstellationFinished;
			}
		}
//		GenerateConstellation ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!isGameStarted) {
			if (Time.time - timestampGameStarted >= 10f) {

				for (int i = 0; i < listOfHUD.Length; i++) {
					listOfHUD [i].SetActive (true);
				}

//				GenerateConstellation ();
				Destroy (logo);

				isGameStarted = true;
				activeConstellation = new List<GameObject> ();

				timestampGameStarted = Time.time;

				if (TutorialManager.instance != null && TutorialManager.isTutorialActive) {
					TutorialManager.instance.StartTutorial ();
					TutorialManager.instance.OnTutorialProgressing += HandleOnTutorialProgressing;
				}
			}
		} else {
			if (isTimetrialMode) {
				if (!isTimetrialStarted && Time.time - timestampGameStarted >= TIMETRIAL_START_DELAY) {
					timestampStartTimetrial = Time.time;
					isTimetrialStarted = true;

					textHint.gameObject.SetActive (false);
					panelTime.SetActive (true);
					textTime.gameObject.SetActive (true);

					if (!isTimetrialChallenge) {
						GenerateConstellation ();
					}
				} else if (isTimetrialStarted) {
					if (Time.time - timestampStartTimetrial >= TIMETRIAL_DURATION) {
						for (int i = 0; i < poolOfConstellation.Count; i++) {
							Destroy (poolOfConstellation [i]);
						}

						panelResult.SetActive (true);
						textResult.gameObject.SetActive (true);

						textResult.text = countFinishedConstellation.ToString ();
						ClueManager.instance.gameObject.SetActive (false);
					} else {
						float remainingDuration = Mathf.FloorToInt (TIMETRIAL_DURATION - (Time.time - timestampStartTimetrial));
						textTime.text = Mathf.Floor (remainingDuration / 60f).ToString ("00") + " : " + (remainingDuration % 60f).ToString ("00");
					}
				}
			} else {
				if (!TutorialManager.isTutorialActive && textHint.gameObject.activeSelf && Time.time - timestampGameStarted >= 8f) {
					textHint.gameObject.SetActive (false);
				}
			}
		}

		if (Input.GetKeyUp (KeyCode.Escape)) {
			Application.Quit ();
		}
	}

	public void GenerateConstellation() {
		if (activeConstellation == null) {
			activeConstellation = new List<GameObject> ();
		} else {
			int startIndex = 0;

			if (!hasJustFinishedTutorial) {
				for (int i = 0; i < poolOfConstellation.Count; i++) {
					Destroy (poolOfConstellation [i]);
				}

				poolOfConstellation.Clear ();
				activeConstellation.Clear ();

				countFinishedConstellation = 0;
				ClueManager.instance.ResetCompletedList ();
			} else {
				startIndex = 1;

				countFinishedConstellation = 1;
			}

			for (int i = startIndex; i < listOfConstellation.Length; i++) {
				GameObject newConstellation = (GameObject)Instantiate (listOfConstellation [i]);
				newConstellation.GetComponent<Constellation> ().OnConstellationFinished += HandleOnConstellationFinished;
				newConstellation.SetActive (false);

				poolOfConstellation.Add (newConstellation);
			}
		}

		textProgress.text = countFinishedConstellation.ToString () + " / " + listOfConstellation.Length.ToString ();

		int idx = 0;

		if (hasJustFinishedTutorial) {
			idx = 1;
		}

		while ( idx < listOfConstellation.Length ) {
			bool isSpawnSuccess = SpawnConstellation (idx, Vector3.zero);

			if (isSpawnSuccess) {
				++idx;
			} else {
				if (hasJustFinishedTutorial) {
					for (int i = 1; i < activeConstellation.Count; i++) {
						poolOfConstellation [i].SetActive (false);
						activeConstellation.RemoveAt (i);
					}

					idx = 1;
				} else {
					for (int i = 0; i < poolOfConstellation.Count; i++) {
						poolOfConstellation [i].SetActive (false);
					}

					activeConstellation.Clear ();
					idx = 0;
				}
			}
		}

		if (hasJustFinishedTutorial) {
			hasJustFinishedTutorial = false;
		}
	}

	void HandleOnTutorialProgressing(int step) {
		if (step == 2) {
			GameObject newConstellation = (GameObject)Instantiate (listOfConstellation [0]);
			newConstellation.GetComponent<Constellation> ().OnConstellationFinished += HandleOnConstellationFinished;
			newConstellation.SetActive (false);

			poolOfConstellation.Add (newConstellation);

			ClueManager.instance.ChangeClue (0);

			SpawnConstellation (0, new Vector3 (-0.35f, 0.25f, 1f));
		} else if (step == 10) {
			hasJustFinishedTutorial = true;
			timestampGameStarted = Time.time;

			if (isTimetrialChallenge) {
				UnityEngine.SceneManagement.SceneManager.LoadScene ("Mainmenu");
			} else {
				GenerateConstellation ();
			}
		}
	}

	bool SpawnConstellation(int index, Vector3 spesificUnitPosition) {
		GameObject newConstellation = poolOfConstellation [index];
		newConstellation.SetActive (true);
		newConstellation.transform.SetParent (transform);

		bool isOverlapping = false;
		Vector3 constellationMoveDirection = new Vector3 ();
		int numOfTry = 0;

		do {
			newConstellation.GetComponent<Constellation>().isSomethingOverlapping = false;

			if (spesificUnitPosition != Vector3.zero) {
				constellationMoveDirection = spesificUnitPosition;
			} else {
				constellationMoveDirection = Random.onUnitSphere;

				bool isPositionValid = true;

				do {
					if (constellationMoveDirection.y < -0.5f || constellationMoveDirection.y > 0.6f) {
						isPositionValid = false;
						constellationMoveDirection = Random.onUnitSphere;
					} else {
						isPositionValid = true;
					}
				} while (!isPositionValid);
			}

			newConstellation.transform.localPosition = constellationMoveDirection * 3.5f;
			newConstellation.transform.LookAt (transform);

			if (spesificUnitPosition != Vector3.zero) {
				isOverlapping = false;
			} else {
//				isOverlapping = newConstellation.GetComponent<Constellation>().isSomethingOverlapping;
				isOverlapping = false;

				for (int i = 0; i < activeConstellation.Count; i++) {
					if (newConstellation.GetComponent<BoxCollider>().bounds.Intersects(activeConstellation[i].GetComponent<BoxCollider>().bounds)) {
						isOverlapping = true;
						break;
					}
				}
			}

			++numOfTry;

			if (numOfTry > 100) {
				return false;
			}
		} while (isOverlapping);

		activeConstellation.Add (newConstellation);

		return true;
	}

	void HandleOnConstellationFinished(Constellation constellation) {
		++countFinishedConstellation;

		textProgress.text = countFinishedConstellation.ToString () + " / " + listOfConstellation.Length.ToString ();

		if (isTimetrialChallenge) {
			ClueManager.instance.NextClue ();
		} else {
			ClueManager.instance.ChangeClue ();
		}
	}

	public bool IsObjectOverlappingWithConstellation(Bounds objectBounds) {
		for (int i = 0; i < activeConstellation.Count; i++) {
			if (objectBounds.Intersects(activeConstellation[i].GetComponent<BoxCollider>().bounds)) {
				return true;
			}
		}

		return false;
	}

	void HandleOnSoundIntroFinished() {
		if (!isAmbiencePlaying) {
			isAmbiencePlaying = true;
			soundAmbience.Play ();

			isBGMPlaying = true;
			soundBGM.PlayDelayed (10f);
		}
	}

	void HandleOnSoundAmbienceFinished() {
		if (!isBGMPlaying) {
			isBGMPlaying = true;
//			soundBGM.Play ();
		}
	}
}
