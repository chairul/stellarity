﻿using UnityEngine;
using System.Collections;

public class SoundManager {
	
	public static bool isSfxOn;
	public static bool isBgmOn;
	public static AudioSource bgm;
//	public static BGMIntroObject bgmIntro;
	public static AudioClip[] clipSFXStarLine;
	public static AudioClip clipSFXCrosshair;
	public static AudioClip clipSFXButton;
	public static AudioClip clipSFXWrong;
	public static AudioClip clipSFXFanfare;
//	public static AudioClip[] clipsVODinoName;
//	public static AudioClip[] clipsSFXDinoSound;

	public static GameObject referenceCamera;

	private static GameObject prefabSfxOneShot;
	private static GameObject vo;
	
	public static void Initialize() {
		/* === Create AudioSource object for playing BGM === */
//		GameObject bgmObject = (GameObject)MonoBehaviour.Instantiate (Resources.Load<GameObject>("Prefabs/BGMObject"));
//		bgm = bgmObject.GetComponent<AudioSource> ();
//		bgm.mute = !isBgmOn;
//		bgm.volume = 0.7f;
//		MonoBehaviour.DontDestroyOnLoad (bgm.gameObject);

//		bgmObject = (GameObject)MonoBehaviour.Instantiate (Resources.Load<GameObject>("Prefabs/BGMIntroObject"));
//		bgmIntro = bgmObject.GetComponent<BGMIntroObject> ();
//		bgmIntro.GetComponent<AudioSource>().mute = !isBgmOn;
//		bgmIntro.GetComponent<AudioSource> ().volume = 0.7f;
//		bgmIntro.objectBGM = bgm;
//		MonoBehaviour.DontDestroyOnLoad (bgmIntro.gameObject);
		/* === === */

		/* === Load SFX One Shot Prefab === */
		prefabSfxOneShot = Resources.Load<GameObject> ("Prefabs/SFXOneShot");
		/* === === */
		
		/* === Load Audio Clip === */
		clipSFXStarLine = Resources.LoadAll<AudioClip>("SFX/StarLine");
		clipSFXCrosshair = Resources.Load<AudioClip> ("SFX/sfx_crosshair");
		clipSFXWrong = Resources.Load<AudioClip> ("SFX/sfx_wrong");
		clipSFXButton = Resources.Load<AudioClip> ("SFX/sfx_button");
		clipSFXFanfare = Resources.Load<AudioClip> ("SFX/sfx_fanfare");
//		clipVOSelectGameplay = Resources.Load<AudioClip> ("VO/vo_select_gameplay");
//		clipVOSelectPuzzle = Resources.Load<AudioClip> ("VO/vo_select_puzzle");
//		clipSFXGeneralButton = Resources.Load<AudioClip> ("SFX/sfx_general_button");
//		clipSFXHomeButton = Resources.Load<AudioClip> ("SFX/sfx_home_button");
//		clipSFXFeedbackCorrect = Resources.Load<AudioClip> ("SFX/sfx_feedback_correct");
//		clipSFXFeedbackWrong = Resources.Load<AudioClip> ("SFX/sfx_feedback_wrong");
//		clipsVOPositiveFeedback = Resources.LoadAll<AudioClip> ("VO/Feedback Positive");
//		clipsVONegativeFeedback = Resources.LoadAll<AudioClip> ("VO/Feedback Negative");
//		clipsVOResult = Resources.LoadAll<AudioClip> ("VO/Result");
//		clipsVODinoName = Resources.LoadAll<AudioClip> ("VO/Dino Name");
//		clipsSFXDinoSound = Resources.LoadAll<AudioClip> ("VO/Dino Sound");
		/* === === */
	}
	
	public static void PlayBGM() {
//		if (bgm.clip == null || bgm.clip.name != bgmClip.name) {
//			bgm.clip = bgmClip;
//			bgm.volume = 0.4f;
//			bgm.Play ();
//		}

//		bgmIntro.Play ();
	}
	
	public static void PlaySFXOneShot(AudioClip sfxClip, bool isOnVRMode = false) {
		if (isSfxOn) {
			GameObject sfxOneShot = (GameObject)MonoBehaviour.Instantiate (prefabSfxOneShot);
			
			if (sfxClip.name.Contains("arsakids")) {
//				if (vo != null) {
//					GameObject.Destroy(vo);
//				}
//				
//				vo = sfxOneShot;
				
				sfxOneShot.GetComponent<AudioSource> ().volume = SoundVolumeAdjuster.volumeSFXArsakids; // 0.5f;
			} else if (sfxClip.name == "sfx_crosshair") {
				sfxOneShot.GetComponent<AudioSource> ().volume = SoundVolumeAdjuster.volumeSFXCrosshair; // 0.7f;
//				GameObject.DontDestroyOnLoad(sfxOneShot);
			} else if (sfxClip.name == "sfx_button") {
				sfxOneShot.GetComponent<AudioSource> ().volume = SoundVolumeAdjuster.volumeSFXButton; // 0.7f;
			} else {
				sfxOneShot.GetComponent<AudioSource> ().volume = SoundVolumeAdjuster.volumeSFXGeneral; // 0.7f;
//				GameObject.DontDestroyOnLoad(sfxOneShot);
			}

			if (isOnVRMode) {
				sfxOneShot.transform.SetParent (referenceCamera.transform);
				sfxOneShot.transform.localPosition = new Vector3 ();
			}
			
			sfxOneShot.GetComponent<SFXOneShot> ().Play (sfxClip);
		}
	}
}
