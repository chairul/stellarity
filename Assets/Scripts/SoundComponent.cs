﻿using UnityEngine;
using System.Collections;

public class SoundComponent : MonoBehaviour {

	public delegate void AudioFinished ();
	public event AudioFinished OnAudioFinished;

	public bool isLooping;

	private bool isPlaying;
	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!isPlaying) {
			if (audioSource.isPlaying) {
				isPlaying = true;
			}
		} else {
			if (!audioSource.isPlaying) {
//				isPlaying = false;

				if (OnAudioFinished != null) {
					OnAudioFinished ();
				}

				if (isLooping) {
					audioSource.loop = true;
					audioSource.Play ();
				}
			}
		}
	}
}
