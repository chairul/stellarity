﻿using UnityEngine;
using System.Collections;

public class SoundVolumeAdjuster : MonoBehaviour {

	public static float volumeSFXGeneral;
	public static float volumeSFXCrosshair;
	public static float volumeSFXArsakids = 1f;
	public static float volumeSFXButton;

	public float volSFXGeneral;
	public float volSFXCrosshair;
	public float volSFXArsakids;
	public float volSFXButton;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);

		volSFXArsakids = 1f;
		volSFXGeneral = 0.3f;
		volSFXCrosshair = 0.1f;
		volSFXButton = 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
		volumeSFXGeneral = volSFXGeneral;
		volumeSFXCrosshair = volSFXCrosshair;
		volumeSFXArsakids = volSFXArsakids;
		volumeSFXButton = volSFXButton;
	}
}
