﻿using UnityEngine;
using System.Collections;

public class StarLine : MonoBehaviour {

	public delegate void FinishCreatingLine ();
	public event FinishCreatingLine OnFinishCreatingLine;

	private static int previousSfxIndex = -1;

	private const int NUM_OF_STEP = 10;

	private int countStep;
	private bool isCreatingLine;
	private bool isReducingOpacity;
	private Vector3 destination;
	private Vector3 currentEndLine;
	private Vector3 progressingSpeed;
	private LineRenderer lineRenderer;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (isCreatingLine) {
			ProgressingLineCreation ();
		} else if (isReducingOpacity) {
			ProgressingReduceOpacity ();
		}
	}

	public void CreateLine(Vector3 src, Vector3 dest) {
		lineRenderer = GetComponent<LineRenderer> ();

		isCreatingLine = true;
		lineRenderer.SetPosition (0, src);
		lineRenderer.SetPosition (1, src);

		destination = dest;
		currentEndLine = src;
		progressingSpeed = (dest - src) / NUM_OF_STEP;
		countStep = 0;

		int sfxIndex = Random.Range(0, SoundManager.clipSFXStarLine.Length);

		while (sfxIndex == previousSfxIndex) {
			sfxIndex = Random.Range(0, SoundManager.clipSFXStarLine.Length);
		}

		previousSfxIndex = sfxIndex;
		SoundManager.PlaySFXOneShot (SoundManager.clipSFXStarLine [sfxIndex], true);
	}

	public void ProgressingLineCreation() {
		currentEndLine += progressingSpeed;
		lineRenderer.SetPosition (1, currentEndLine);

		++countStep;
		if (countStep >= NUM_OF_STEP) {
			isCreatingLine = false;

			if (OnFinishCreatingLine != null) {
				Debug.Log ("Finish Creating Line " + gameObject.name);
				OnFinishCreatingLine ();
			}
		}
	}

	public void ReduceOpacity() {
		isReducingOpacity = true;
		countStep = 0;

//		Color newColor = new Color (1f, 1f, 1f, 0.25f);
//		lineRenderer.SetColors (newColor, newColor);
	}

	public void ProgressingReduceOpacity() {
		++countStep;
		Color newColor = new Color (1f, 1f, 1f, 1f - (0.75f / (60 - countStep)));
		lineRenderer.SetColors (newColor, newColor);

		if (countStep == 60) {
			isReducingOpacity = false;
		}
	}
}
