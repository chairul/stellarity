﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Constellation))]
public class ConstellationEditor : Editor {

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();

		Constellation constellation = (Constellation)target;
		if (GUILayout.Button ("Look to center")) {
			constellation.LookToCenter ();
		}
	}
}
