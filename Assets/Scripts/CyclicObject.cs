﻿using UnityEngine;
using System.Collections;

public class CyclicObject : MonoBehaviour {

	public GameObject target;
	public float durationAwake;
	public float durationSleep;

	private bool isAwake;
	private float timestampCycleStarted;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable () {
		isAwake = true;
		timestampCycleStarted = Time.time;

	}
}
