﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TriviaManager : MonoBehaviour {

	public GameObject[] character;
	public GameObject UINotif;
	public GameObject UITrivia;
	public SpaceObjectGenerator spaceObjectManager;

	private const float APPEAR_DURATION = 60f;
	private const float DISAPPEAR_DURATION = 60f;

	private string[] contentTrivia = { "Matahari kita sebenarnya adalah bintang, masih banyak 'matahari' lain tersebar di seantero semesta", 
		"Bintang yang paling 'dingin' berwarna merah, malah bintang yang terpanas warnanya biru!",
		"Bintang terdiri dari 74% hidrogen dan 25% helium",
		"Rasi bintang adalah sekumpulan bintang yang terlihat berkelompok di langit dari sudut pandang di Bumi dan sering diimajinasikan menjadi simbol gambar",
		"Rasi bintang berubah-ubah sesuai dengan rotasi bumi. Rasi bintang pada zaman dulu digunakan untuk penunjuk arah",
		"Kamu tidak dapat mendengar suara di luar angkasa, jadi tidak ada yang akan mendengar kamu kentut. hihi",
		"Orang pertama yang melihat ke luar angkasa menggunakan teleskop adalah Galileo, kurang lebih 400 tahun yang lalu.",
		"Makhluk bumi pertama yang dikirim keluar angkasa adalah seekor anjing bernama Laika. Woof!",
		"Wanita pertama yang dikirim keluar angkasa adalah Valentina Tereshkova, keren ya!",
		"Tata surya kita yang berisi matahari dan planet-planet sangat kecil jika dibandingkan dengan alam semesta",
		"Matahari sangat besar. Matahari bisa memuat satu juta bumi",
		"Bumi yang kita tinggali ini sudah berusia milyaran tahun. Mari kita jaga dan lindungi bersama."};
	private int previousContentIndex;
	private int characterIndex;
	private float timestampStartAppearing;
	private float timestampStartDisappearing;
	private bool isAppearing;

	// Use this for initialization
	void Start () {
		previousContentIndex = -1;

		Appear ();
	}

	// Update is called once per frame
	void Update () {
		if (isAppearing) {
			if (Time.time - timestampStartAppearing >= APPEAR_DURATION) {
				Disappear ();
			}
		} else {
			if (Time.time - timestampStartDisappearing >= DISAPPEAR_DURATION) {
				Appear ();
			}
		}
	}

	public void Appear() {
		isAppearing = true;
		timestampStartAppearing = Time.time;

		characterIndex = Random.Range (0, 2);
		character [characterIndex].SetActive (true);

		UINotif.SetActive (true);

		Bounds objectBounds = GetComponent<BoxCollider> ().bounds;
		bool isOverlappingWithConstellation = false;

		do {
			Vector3 direction = Random.onUnitSphere;

			while (direction.y < -0.5f) {
				direction = Random.onUnitSphere;
			}

			transform.localPosition = direction * 4f;
			transform.LookAt (new Vector3 (0f, 1f, 0f));

//			isOverlappingWithConstellation = spaceObjectManager.IsObjectOverlappingWithConstellation (objectBounds);
		} while (isOverlappingWithConstellation);
	}

	public void Disappear() {
		isAppearing = false;
		timestampStartDisappearing = Time.time;

		character [characterIndex].SetActive (false);
		UINotif.SetActive (false);
		UITrivia.SetActive (false);
	}

	public void DisplayTrivia() {
		UINotif.SetActive (false);
		UITrivia.SetActive (true);

		int contentIndex = Random.Range (0, contentTrivia.Length);

		while (contentIndex == previousContentIndex) {
			contentIndex = Random.Range (0, contentTrivia.Length);
		}

		UITrivia.GetComponentInChildren<Text> ().text = contentTrivia [contentIndex];
	}
}
