﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Constellation : MonoBehaviour {

	public delegate void ConstellationFinished(Constellation constellation);
	public event ConstellationFinished OnConstellationFinished;

	public int constellationIndex;
	public Star[] listOfStar;
	public GameObject prefabStarLine;
	public SpriteRenderer constellationImage;
	public SpriteRenderer constellationName;
	public bool isSomethingOverlapping;

	private List<int> listOfActivatedStarIndex;
	private List<StarLine> listOfStarLine;
	private bool isFinished;
	private bool isProcessingImage;
	private bool isProcessingName;
	private int countProcessingImageStep;

	// Use this for initialization
	void Start () {
		listOfActivatedStarIndex = new List<int> ();
		listOfStarLine = new List<StarLine> ();
		isFinished = false;
	}

	// Update is called once per frame
	void Update () {
		if (isProcessingImage) {
			ProcessingImage ();
		} else if (isProcessingName) {
			ProcessingName ();
		}
	}

	public void StarSelected(Star star) {
		// Check if any of star's neighbour is activated
		bool isNeighbourActivated = false;
		int neighbourIndex = -1;

		if (star.starIndex == 0) {
			if (constellationIndex == ClueManager.prevClueIndex) {
				isNeighbourActivated = true;
			}
		} else {
			for (int i = 0; i < listOfActivatedStarIndex.Count; i++) {
				if (star.isStarANeighbour (listOfActivatedStarIndex [i])) {
					isNeighbourActivated = true;
					neighbourIndex = listOfActivatedStarIndex [i];
					break;
				}
			}
		}

		// Activate star if neighbour is activated
		if (isNeighbourActivated) {
			star.ActivateStar ();
			listOfActivatedStarIndex.Add (star.starIndex);

			if (star.starIndex > 0) {
				for (int i = 0; i < star.listOfNeighbourStarIndex.Length; i++) {
					if (star.listOfNeighbourStarIndex[i] == neighbourIndex || listOfActivatedStarIndex.Contains (star.listOfNeighbourStarIndex [i])) {
						GameObject starLine = (GameObject)Instantiate (prefabStarLine);
						starLine.transform.SetParent (transform);
						starLine.GetComponent<StarLine> ().CreateLine (listOfStar [star.listOfNeighbourStarIndex[i]].transform.position, star.transform.position);

						listOfStarLine.Add (starLine.GetComponent<StarLine> ());

						// Finish constellation if all star has been activated
						if (listOfActivatedStarIndex.Count == listOfStar.Length) {
							if (i == star.listOfNeighbourStarIndex.Length - 1) {
								isFinished = true;
								starLine.GetComponent<StarLine> ().OnFinishCreatingLine += HandleOnFinishCreatingLine;

								if (!TutorialManager.isTutorialActive) {
									if (OnConstellationFinished != null) {
										OnConstellationFinished (this);
									}

									ClueManager.instance.AddIndexToCompletedList (constellationIndex);
								}

								SoundManager.PlaySFXOneShot (SoundManager.clipSFXFanfare, true);
							}
						}
					}
				}
			}
		} else {
			FeedbackWrong.DisplayFeedback ();
			SoundManager.PlaySFXOneShot (SoundManager.clipSFXWrong, true);
		}
	}

	void HandleOnFinishCreatingLine() {
		for (int i = 0; i < listOfStar.Length; i++) {
			listOfStar [i].gameObject.SetActive (false);
		}

		for (int i = 0; i < listOfStarLine.Count; i++) {
			listOfStarLine [i].ReduceOpacity ();
		}

		constellationImage.gameObject.SetActive (true);
		isProcessingImage = true;
		countProcessingImageStep = 0;

		if (TutorialManager.isTutorialActive) {
			TutorialManager.instance.TriggerProgressingTutorial (7);
		}
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.collider.gameObject.CompareTag ("Nonoverlapping")) {
			isSomethingOverlapping = true;
		}
	}

	void ProcessingImage() {
		++countProcessingImageStep;

		Color imageColor = constellationImage.color;
		imageColor.a =  (float) countProcessingImageStep / (float) 60;
		constellationImage.color = imageColor;

		if (countProcessingImageStep == 60) {
			isProcessingImage = false;
			isProcessingName = true;

			imageColor = constellationName.color;
			imageColor.a =  0;
			constellationName.color = imageColor;
			constellationName.gameObject.SetActive (true);

			countProcessingImageStep = 0;
		}
	}

	void ProcessingName() {
		++countProcessingImageStep;

		Color imageColor = constellationName.color;
		imageColor.a =  (float) countProcessingImageStep / (float) 30;
		constellationName.color = imageColor;

		constellationName.gameObject.SetActive (true);

		if (countProcessingImageStep == 30) {
			isProcessingName = false;
		}
	}

	public void LookToCenter() {
		transform.LookAt (GameObject.Find ("Space Object Manager").transform);
	}
}
