﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Star : InteractiveObject {

	public int starIndex;
	public int[] listOfNeighbourStarIndex;

	private bool isActive;
	private Constellation constellation;

	// Use this for initialization
	void Start () {
		constellation = transform.parent.GetComponent<Constellation> ();
		isInteractable = true;
	}

	protected override void ActionOnSelected () {
		if (!isActive) {
			constellation.StarSelected (this);
		}
	}

	protected override void ActionOnGazed (float thresholdComparison) {
		
	}

	protected override void ActionOnGazeStarted() {
		if (starIndex == 0 && TutorialManager.instance != null && TutorialManager.isTutorialActive) {
			TutorialManager.instance.TriggerProgressingTutorial (4);
		}
	}

	public bool isStarANeighbour(int neighbourStarIndex) {
		for (int i = 0; i < listOfNeighbourStarIndex.Length; i++) {
			if (neighbourStarIndex == listOfNeighbourStarIndex [i]) {
				return true;
			}
		}

		return false;
	}

	public void ActivateStar() {
		if (!isActive) {
			isActive = true;

//			GetComponent<SpriteRenderer> ().color = new Color (1f, 1f, 1f, 1f);
			GetComponent<Animator>().SetTrigger("Select");

			if (starIndex == 0 && TutorialManager.isTutorialActive) {
				TutorialManager.instance.TriggerProgressingTutorial (5);
			}

			isInteractable = false;
		}
	}

	public void ResetStar() {
		isInteractable = true;
		isActive = false;

		GetComponent<Animator> ().SetTrigger ("Reset");
	}
}
