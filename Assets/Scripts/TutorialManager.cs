﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TutorialManager : MonoBehaviour {

	public static TutorialManager instance;

	public delegate void TutorialProgressing(int step);
	public event TutorialProgressing OnTutorialProgressing;

	public Text textInstruction;
	public GameObject avatarPippo;
	public GameObject pippoTrivia;
	public GameObject floorMenu;
	public ClueManager clueManager;

	private string[] contentTutorial = {"Halo, selamat bermain di STELLARITY! Kali ini Pippo akan mengajakmu mencari rasi bintang di langit",
		"Lihat di bawah! Sepertinya ada rasi bintang yang harus kamu cari!",
		"Wah kira-kira bentuk apa itu ya? Baiklah ayo kita cari di langit!",
		"Pertama-tama carilah dari bintang berbentuk segi lima",
		"Arahkan titik sasarmu ke bintang itu ya sampai bintangmu bersinar!",
		"Tahan sampai bulatan sasarannya penuh ya",
		"Wah, tepat sekali!",
		"Sekarang carilah bintang selanjutnya...Ahh...sepertinya kamu harus menghubungkan secara berurutan!",
		"Wah ternyata rasi itu bergambar kelinci!",
		"Ayo temukan lagi bintang-bintang lainnya di Mode Bermain!"};
	private const float AUTO_PROGRESS_DELAY = 4f;

	public static bool isTutorialActive;
	private bool isStepAutoProgress;
	private int countTutorialStep;
	private float timestampLastProgress;
	private List<int> listOfAutoProgressingStep = new List<int>() {0, 1, 2, 3, 6, 8, 9};

	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		if (isTutorialActive && isStepAutoProgress && Time.time - timestampLastProgress >= AUTO_PROGRESS_DELAY) {
			isStepAutoProgress = false;
			ProgressingTutorial ();
		}
	}

	void ProgressingTutorial() {
		if (countTutorialStep < 9) {
			++countTutorialStep;

			if (listOfAutoProgressingStep.Contains (countTutorialStep)) {
				SetCurrentStepAsAutoProgress ();
			}

			UpdateDisplayText ();

			if (OnTutorialProgressing != null) {
				OnTutorialProgressing (countTutorialStep);
			}
		} else {
			isTutorialActive = false;
			avatarPippo.SetActive (false);
			pippoTrivia.SetActive (true);
			floorMenu.SetActive (true);

			clueManager.ChangeClue ();

			textInstruction.text = "Carilah rasi bintang yang sesuai dengan petunjuk";

			if (OnTutorialProgressing != null) {
				OnTutorialProgressing (10);
			}
		}
	}

	void UpdateDisplayText() {
		textInstruction.text = contentTutorial [countTutorialStep];
	}

	void SetCurrentStepAsAutoProgress() {
		isStepAutoProgress = true;
		timestampLastProgress = Time.time;
	}

	public void TriggerProgressingTutorial(int step) {
		if (step == countTutorialStep) {
			ProgressingTutorial ();
		} else if (step == 5 && countTutorialStep < step) {
			countTutorialStep = 5;
			ProgressingTutorial ();
		}
	}

	public void StartTutorial() {
//		isTutorialActive = true;

		if (isTutorialActive) {
			clueManager.ChangeClue (0);
			avatarPippo.SetActive (true);
			pippoTrivia.SetActive (false);
			floorMenu.SetActive (false);

			UpdateDisplayText ();
			SetCurrentStepAsAutoProgress ();
		}
	}
}
