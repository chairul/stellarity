﻿using UnityEngine;
using System.Collections;

public class InteractiveObject : MonoBehaviour {

	private const float SELECT_TIME_THRESHOLD = 1.5f;

	public static float timestampLastSfxPlayed;

	private float timestampGazeStarted;
	private bool isBeingGazed;
	private bool hasBeenActivated;

	protected bool isInteractable;

	// Use this for initialization
	void Start () {
		isInteractable = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (isBeingGazed) {
			OnPointerStay ();
		}
	}

	public void OnPointerEnter() {
		if (isInteractable) {
			isBeingGazed = true;
			hasBeenActivated = false;
			timestampGazeStarted = Time.time;

			ActionOnGazeStarted ();

			timestampLastSfxPlayed = Time.time;
			SoundManager.PlaySFXOneShot (SoundManager.clipSFXCrosshair, true);
		}
	}

	public void OnPointerStay() {
		if (!hasBeenActivated) {
			float gazeDuration = Time.time - timestampGazeStarted;

			if (gazeDuration >= SELECT_TIME_THRESHOLD) {
				ActionOnSelected ();
				Crosshair.instance.AdjustGazeFeedback (0f);
				timestampGazeStarted = Time.time;

				hasBeenActivated = true;
			} else {
				ActionOnGazed (gazeDuration / SELECT_TIME_THRESHOLD);
				Crosshair.instance.AdjustGazeFeedback (gazeDuration / SELECT_TIME_THRESHOLD);
			}

			if (Time.time - timestampLastSfxPlayed >= 1f) {
				SoundManager.PlaySFXOneShot (SoundManager.clipSFXCrosshair, true);
				timestampLastSfxPlayed = Time.time;
			}
		}
	}

	public void OnPointerExit() {
		isBeingGazed = false;
		Crosshair.instance.AdjustGazeFeedback (0f);

		ActionOnGazeEnded ();
	}

	protected virtual void ActionOnGazeStarted() {
	
	}

	protected virtual void ActionOnGazeEnded() {

	}

	protected virtual void ActionOnSelected() {
		
	}

	protected virtual void ActionOnGazed(float thresholdComparison) {
		
	}
}
