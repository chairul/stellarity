﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ClueManager : MonoBehaviour {

	public Sprite[] listOfClueSprite;
	public Image imageClue;
	public bool isClueOrdered;

	public static int prevClueIndex;
	public static ClueManager instance;

	private List<int> listOfCompletedConstellationIndex;

	// Use this for initialization
	void Start () {
		instance = this;
		prevClueIndex = -1;
		listOfCompletedConstellationIndex = new List<int> ();

		if (TutorialManager.instance == null || !TutorialManager.isTutorialActive) {
			if (isClueOrdered) {
				NextClue ();
			} else {
				ChangeClue ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeClue(int index = -1) {
		int clueIndex = 0;
		bool isIndexValid = false;

		if (listOfCompletedConstellationIndex == null) {
			listOfCompletedConstellationIndex = new List<int> ();
		}

		while (!isIndexValid) {
			isIndexValid = true;
			clueIndex = Random.Range (0, listOfClueSprite.Length);

			if (clueIndex == prevClueIndex) {
				isIndexValid = false;
			} else if (listOfCompletedConstellationIndex.Contains(clueIndex)) {
				isIndexValid = false;
			}
		}

		if (index != -1) {
			clueIndex = index;
		}

		imageClue.sprite = listOfClueSprite [clueIndex];

		prevClueIndex = clueIndex;
	}

	public void NextClue() {
		Debug.Log (prevClueIndex);
		++prevClueIndex;
		imageClue.sprite = listOfClueSprite [prevClueIndex];
	}

	public void AddIndexToCompletedList(int constellationIndex) {
		listOfCompletedConstellationIndex.Add (constellationIndex);
	}

	public void ResetCompletedList() {
		listOfCompletedConstellationIndex.Clear ();
	}
}
