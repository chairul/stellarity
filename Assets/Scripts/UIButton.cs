﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIButton : InteractiveObject {

	public SpaceObjectGenerator spaceObjectGenerator;
	public Sprite spriteActive;
	public Sprite spriteInactive;

	protected override void ActionOnSelected () {
		spaceObjectGenerator.GenerateConstellation ();
		GetComponent<Image> ().sprite = spriteInactive;
		SoundManager.PlaySFXOneShot (SoundManager.clipSFXButton, true);
	}

	protected override void ActionOnGazeStarted() {
		Crosshair.instance.SetDarkMode ();
		GetComponent<Image> ().sprite = spriteActive;
	}

	protected override void ActionOnGazeEnded () {
		Crosshair.instance.SetLightMode ();
		GetComponent<Image> ().sprite = spriteInactive;
	}
}
