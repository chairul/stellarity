﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Splashscreen : SceneManager {
	
	private readonly float SPLASH_TIME = 2.5f;

	private bool isVOPlayed;

	public Image splashLennovo;
	public GameObject logo;
	public AudioClip clipSfxSplashscreen;
	
	/*
	 * Initialization
	 */
	void Start() {
		/* === Load game data === */
		DataManager.LoadData ();
		/* === === */
		
		/* === Initialize General Component === */
		SoundManager.Initialize ();
		/* === === */

		SoundManager.PlaySFXOneShot (clipSfxSplashscreen);
	}
	
	/*
	 * Customized update call
	 */
	protected override void OnUpdateCalled () {
		if (!isVOPlayed && Time.timeSinceLevelLoad >= 1f) {
			/* === Play Splashscreen VO === */
			SoundManager.PlaySFXOneShot (Resources.Load<AudioClip> ("SFX/vo_splashscreen_arsakids"));
			isVOPlayed = true;
			/* === === */
		}

//		if (Time.timeSinceLevelLoad >= 2f && splashLennovo.gameObject.activeSelf) {
//			splashLennovo.gameObject.SetActive (false);
//			logo.SetActive (true);
//
//			SoundManager.PlaySFXOneShot (clipSfxSplashscreen);
//		}
		
		if (Time.timeSinceLevelLoad >= SPLASH_TIME) {
			SoundManager.PlayBGM ();
			Application.LoadLevel("Mainmenu");
		}
	}
}
