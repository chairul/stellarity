﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIClueButton : InteractiveObject {

	public ClueManager clueManager;
	public Sprite spriteActive;
	public Sprite spriteInactive;

	protected override void ActionOnSelected () {
		clueManager.ChangeClue ();
		GetComponent<Image> ().sprite = spriteInactive;
		SoundManager.PlaySFXOneShot (SoundManager.clipSFXButton, true);
	}

	protected override void ActionOnGazed (float thresholdComparison) {

	}

	protected override void ActionOnGazeStarted() {
		GetComponent<Image> ().sprite = spriteActive;
	}

	protected override void ActionOnGazeEnded () {
		GetComponent<Image> ().sprite = spriteInactive;
	}
}
