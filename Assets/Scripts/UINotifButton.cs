﻿using UnityEngine;
using System.Collections;

public class UINotifButton : InteractiveObject {

	private TriviaManager triviaManager;

	// Use this for initialization
	void Start () {
		triviaManager = transform.parent.GetComponent<TriviaManager> ();
		isInteractable = true;
	}

	protected override void ActionOnSelected () {
		// Display Trivia
		triviaManager.DisplayTrivia();
		SoundManager.PlaySFXOneShot (SoundManager.clipSFXButton, true);
	}

	protected override void ActionOnGazed (float thresholdComparison) {

	}

}
